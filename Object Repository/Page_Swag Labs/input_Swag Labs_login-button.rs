<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Swag Labs_login-button</name>
   <tag></tag>
   <elementGuidId>8090028e-b9bc-4d5a-9560-b3c8ce5375af</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='login-button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#login-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f209cf83-59c8-4399-9186-2a8b3f68de8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>c5b7ed04-2cd2-4c5c-beef-b58b784025f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>submit-button btn_action</value>
      <webElementGuid>240feea1-aa44-49c6-8fe3-4f4a8713e871</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>1f1d6100-da31-4a91-ac39-7ba0315352c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>a4538f9d-c933-45bb-b6ca-350526eeef4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>login-button</value>
      <webElementGuid>837317b0-b17f-473a-ac4e-767997d6baf8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Login</value>
      <webElementGuid>a0d2bf21-ff1b-45f3-ad9e-3f10dbea46e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login-button&quot;)</value>
      <webElementGuid>6de98504-f436-433a-bcc2-2b3845698a00</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='login-button']</value>
      <webElementGuid>75b1322b-c639-4682-83f0-b1dc4b2a5085</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='login_button_container']/div/form/input</value>
      <webElementGuid>40a20bc1-d6a9-4a49-8545-2ebdb79e6e49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/input</value>
      <webElementGuid>eee138f3-91aa-4a9f-a06c-25daf9558b41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @id = 'login-button' and @name = 'login-button']</value>
      <webElementGuid>c066a460-e5a2-4af6-b437-2a664bb90b1e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
