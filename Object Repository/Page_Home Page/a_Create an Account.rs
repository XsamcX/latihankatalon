<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Create an Account</name>
   <tag></tag>
   <elementGuidId>6252456d-e428-4e25-b87a-713d39f983c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li:nth-of-type(3) > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Create an Account')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>1efb748d-ef18-49c8-8614-bebf55a12530</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://magento.softwaretestingboard.com/customer/account/create/</value>
      <webElementGuid>74277e79-e7c8-4ebf-842f-5f08b450b8fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create an Account</value>
      <webElementGuid>a6f0bc7a-86f8-432b-833e-a9af60a4e0a4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;cms-home cms-index-index page-layout-1column&quot;]/div[@class=&quot;page-wrapper&quot;]/header[@class=&quot;page-header&quot;]/div[@class=&quot;panel wrapper&quot;]/div[@class=&quot;panel header&quot;]/ul[@class=&quot;header links&quot;]/li[3]/a[1]</value>
      <webElementGuid>b8c865b5-c121-4f59-9ba9-da2035841182</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Create an Account')]</value>
      <webElementGuid>03caa265-7eac-4305-aa3e-874bfcb5c9fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign In'])[1]/following::a[1]</value>
      <webElementGuid>95131f7a-4491-44be-bfbe-f5cc5d38d9b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Skip to Content'])[1]/following::a[2]</value>
      <webElementGuid>5a6e131b-fdd2-4c97-904e-d7463f1fe24f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Toggle Nav'])[1]/preceding::a[1]</value>
      <webElementGuid>8041966f-bda6-4376-8d16-02f9de1c4ed6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Cart'])[1]/preceding::a[2]</value>
      <webElementGuid>dc715101-f7e5-4fa5-a384-c5f972df71e0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Create an Account']/parent::*</value>
      <webElementGuid>ab1affff-66e1-4200-8db6-cc8c1f551034</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://magento.softwaretestingboard.com/customer/account/create/')]</value>
      <webElementGuid>84f94fc3-3f55-4cac-a11a-275820a2ef3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/a</value>
      <webElementGuid>41e75c7a-ee63-49f3-b8e4-90a1eae38507</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://magento.softwaretestingboard.com/customer/account/create/' and (text() = 'Create an Account' or . = 'Create an Account')]</value>
      <webElementGuid>7c1dc17c-f10b-4518-ba01-631501d04ff5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
