<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Create an Account</name>
   <tag></tag>
   <elementGuidId>685a9e8d-7733-453a-9379-678db7435c3b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form-validate']/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c4427e28-2139-4a57-ae05-2b193454e008</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>primary</value>
      <webElementGuid>4ec17c39-ef04-4b4e-9dd7-2f4ee7db0146</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
Create an Account
</value>
      <webElementGuid>f8c98027-2cc1-41c5-9ead-58dbce92934f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-validate&quot;)/div[@class=&quot;actions-toolbar&quot;]/div[@class=&quot;primary&quot;]</value>
      <webElementGuid>5166407f-c55f-4917-8e92-70d572df29c7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate']/div/div</value>
      <webElementGuid>5d7177b0-aa5d-4929-8198-f7a5796cac18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm Password'])[1]/following::div[3]</value>
      <webElementGuid>466bd729-54d1-4c1e-85bf-9b69741f11f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Very Strong'])[1]/following::div[4]</value>
      <webElementGuid>1d37f938-63a9-4075-bd4a-3953d30897c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/preceding::div[1]</value>
      <webElementGuid>4639c11a-ca51-4e57-bbb2-3c97e1fbdab0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/form/div/div</value>
      <webElementGuid>79e8e0e8-62d8-45ac-a201-663b8b976446</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
Create an Account
' or . = '
Create an Account
')]</value>
      <webElementGuid>a129db5b-9855-4d5f-bca4-142d4a7f03c4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
