<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Create an Account</name>
   <tag></tag>
   <elementGuidId>f946f82f-4f58-43da-b50e-5d8d8f94f959</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.action.submit.primary > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form-validate']/div/div/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>50c812c0-f5bb-47d8-b6de-20525116fac0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Create an Account</value>
      <webElementGuid>69c1ae34-ee0a-4f33-9b28-cd8807b172b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-validate&quot;)/div[@class=&quot;actions-toolbar&quot;]/div[@class=&quot;primary&quot;]/button[@class=&quot;action submit primary&quot;]/span[1]</value>
      <webElementGuid>e43d8781-3c01-4c48-8fbb-c75a56ca4f8d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate']/div/div/button/span</value>
      <webElementGuid>21843d0d-ff98-4e35-8054-1717a446815f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm Password'])[1]/following::span[1]</value>
      <webElementGuid>db6203e2-84ec-4881-aff8-15faccfb2f9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Very Strong'])[1]/following::span[2]</value>
      <webElementGuid>9f610779-6168-46f0-80cd-bd60fa535bcc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back'])[1]/preceding::span[1]</value>
      <webElementGuid>9226a893-3416-4522-b755-acdd21ac6347</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::span[2]</value>
      <webElementGuid>4f43f4ad-d442-454d-9b05-4340f2c20ec6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/button/span</value>
      <webElementGuid>c001f405-d60b-443f-bd48-df7aa81ef4c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Create an Account' or . = 'Create an Account')]</value>
      <webElementGuid>b5b46fda-1f4a-47ee-a53a-f9f830dd6427</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
