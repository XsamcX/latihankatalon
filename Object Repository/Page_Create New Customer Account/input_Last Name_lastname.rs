<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Last Name_lastname</name>
   <tag></tag>
   <elementGuidId>de91c1d8-6d02-4038-a4a4-06baadc08bea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#lastname</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='lastname']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>65bdb488-9171-49c0-bf4a-e384e10600b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>3a5e9c97-2c04-49f9-8116-000a84572952</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>lastname</value>
      <webElementGuid>795aee16-71d3-4cbf-9a7b-04b3871bf8e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>lastname</value>
      <webElementGuid>8190c3cb-800b-41de-9bc9-2657830b4e15</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Last Name</value>
      <webElementGuid>cbb6e796-b68a-4aff-bb1a-983a54b2a283</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>input-text required-entry</value>
      <webElementGuid>dc7e1dca-93d2-4c4f-991f-babb59785ad6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-validate</name>
      <type>Main</type>
      <value>{required:true}</value>
      <webElementGuid>45f6364c-fc30-4cb9-8c17-d9173a89ac61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>86f1f7c1-b0b7-40bf-8752-41fba72d89ed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-required</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>d6657bf0-8aea-480d-87d2-8715d12ef065</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;lastname&quot;)</value>
      <webElementGuid>531e4130-d2aa-4caf-8a53-bee4f58ad005</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='lastname']</value>
      <webElementGuid>3f746d78-fe14-443d-8ff9-a9fae2585735</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='form-validate']/fieldset/div[2]/div/input</value>
      <webElementGuid>7094a68a-c0dc-4b6d-8280-e07ce53319ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/input</value>
      <webElementGuid>bb2d73b9-3791-4f3a-aa08-5ac20628fff4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'lastname' and @name = 'lastname' and @title = 'Last Name']</value>
      <webElementGuid>565bfaf2-7982-4aed-9d94-5f8c034f84e1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
